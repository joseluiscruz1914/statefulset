# StatefulSet

## StatefulSet MongoDB Single Node

```
kubectl apply -f singlenode.yaml
```

## StatefulSet mongoDB ReplicalSet 

### Change replical = 3

```
kubectl apply -f singlenode.yaml
```

```
mongo


rs.initiate({ _id: "MainRepSet", version: 1, 
members: [ 
 { _id: 0, host: "mongod-0.mongodb-service.NAMESPACE.svc.cluster.local:27017" }, 
 { _id: 1, host: "mongod-1.mongodb-service.NAMESPACE.svc.cluster.local:27017" }, 
 { _id: 2, host: "mongod-2.mongodb-service.NAMESPACE.svc.cluster.local:27017" } ]});

```


## Links
- https://kubernetes.io/es/docs/concepts/services-networking/service/#servicios-headless
- https://deep timan.medium.com/mongodb-statefulset-in-kubernetes-87c2f5974821


## Con Helm Chart

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install mongo -f values.yaml bitnami/mongodb -n NOMBRE_GRUPO
```
